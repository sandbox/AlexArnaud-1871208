(function($) {
  $(document).ready(function() {
    if ('search_api_extended_filters' in Drupal.settings) {
      var settings = Drupal.settings.search_api_extended_filters;
      for (id in settings) {
        var identifier = settings[id].identifier || id;
        html_id = 'edit-' + identifier.replace(/_/g, '-');

        var dropdownchecklist_options = {
          icon: {},
        };
        if (settings[id].add_all_option) {
          dropdownchecklist_options.firstItemChecksAll = true;
        }

        $('select#' + html_id + '[multiple]').each(function() {
          var $select = $(this);
          var form_hidden = false;
          if ($select.is(':not(:visible)')) {
            // If select is not visible, jQuery Dropdown CheckList plugin will fail
            // to guess elements width.
            // So we show the form, apply the plugin, and hide again the form.
            $('div.view-filters').show();
            form_hidden = true;
          }

          var max_items = settings[id].max_items_displayed;
          if (max_items) {
            // To calculate correct height and width, we need to apply the plugin,
            // retrieve height and width of elements, then destroy plugin, change
            // options, and re-apply plugin...

            // Calculate maxDropHeight
            max_items = (max_items > 4) ? max_items : 4;
            $select.dropdownchecklist(dropdownchecklist_options);
            var $dropcontainer = $select.siblings('.ui-dropdownchecklist-dropcontainer-wrapper')
              .children('.ui-dropdownchecklist-dropcontainer');
            var $option = $dropcontainer.children('.ui-dropdownchecklist-item').first();
            var max_height = ($option.outerHeight(true)) * max_items;
            dropdownchecklist_options.maxDropHeight = max_height;

            // Calculate width
            var max_width = 0;
            $dropcontainer.children('.ui-dropdownchecklist-item').each(function() {
              var width = 0;
              $(this).children().each(function() {
                width += $(this).outerWidth(true);
              });
              if (max_width < width) {
                max_width = width;
              }
            });

            // Add 25 more pixels to leave room for scrollbar
            dropdownchecklist_options.width = max_width + 25;

            // Destroy plugin
            $select.dropdownchecklist('destroy');
          }

          $select.dropdownchecklist(dropdownchecklist_options);

          if (form_hidden) {
            $('div.view-filters').hide();
          }
        });
      }
    }
  });
})(jQuery);
