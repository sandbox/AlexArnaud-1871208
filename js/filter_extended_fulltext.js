(function($) {

  function search_api_extended_filters_add_filter(conjunction, wrapper) {
    if (conjunction.length > 0) {
      // Clone form elements.
      var filter_container = $(wrapper)
        .find('div.search_api_extended_filters_filter_container:last')
        .clone();
      var filter_conjunction = $(filter_container)
        .find('select.search_api_extended_filters_filter_conjunction').parent();
      var filter_field = $(filter_container)
        .find('select.search_api_extended_filters_filter_field').parent();
      var filter_value = $(filter_container)
        .find('input.search_api_extended_filters_filter_value').parent();

      // Calculate delta.
      var matches = filter_conjunction.find('select').attr('name').match(/\[([0-9]+)\]/);
      var delta = parseInt(matches[1]) + 1;

      // Modify form elements attributes.
      var filters = [filter_conjunction, filter_field, filter_value];
      for (i in filters) {
        filter = filters[i];
        // Modify name.
        name = filter.find('select,input').attr('name');
        name = name.replace(/\[[0-9]+\]/, '[' + delta + ']');
        filter.find('select,input').attr('name', name);
      }

      // Increment ids.
      filter_container.find('[id]').andSelf().each(function() {
        var id = $(this).attr('id');
        matches = id.match(/[0-9]+-([0-9]+)/);
        var idx = parseInt(matches[1]) + 1;
        id = id.replace(/([0-9]+)-[0-9]+/, '$1-' + idx);
        $(this).attr('id', id);
      });

      // Specific treatments.
      filter_conjunction.find('select').removeAttr('disabled').show().val(conjunction);
      filter_value.find('input[type="text"]').val('');
      filter_value.find('input[type="hidden"]').removeClass('autocomplete-processed');

      // Insert form elements into form.
      wrapper.find('select.search_api_extended_filters_add_filter')
        .before(filter_container)
        .val('');

      search_api_extended_filters_enable_autocomplete(filter_container);
    }
  }

  function search_api_extended_filters_on_field_change(field, filter) {
    var input = field.parents('.search_api_extended_filters_filter_container').find('input.search_api_extended_filters_filter_value');
    var input_val = input.val();
    var input_id = input.attr('id');
    var autocomplete_input = input.next();
    var autocomplete_path = autocomplete_input.val();
    var first_time_called = autocomplete_path.match(/PLACEHOLDER$/) ? true : false;

    var fields = [];
    if (field.val() != '') {
      fields.push(field.val());
    } else {
      field.find('option').each(function() {
        if ($(this).val() != '') {
          fields.push($(this).val());
        }
      });
    }

    // Change autocomplete path.
    autocomplete_path = autocomplete_path.replace(/\/[^\/]*$/, '');
    autocomplete_path += '/' + fields.join(' ');
    autocomplete_input.val(autocomplete_path).removeClass('autocomplete-processed');
    input.unbind().val('');
    Drupal.behaviors.autocomplete.attach(filter);

    input.val(input_val);
  }

  function search_api_extended_filters_enable_autocomplete(filter) {
    if (filter.find('input.autocomplete').length > 0) {
      field = filter.find('select.search_api_extended_filters_filter_field');
      field.unbind().bind('change', function() {
        search_api_extended_filters_on_field_change($(this), filter);
      });
      field.change();
    }
  }

  $(document).ready(function($) {
    $('.search_api_extended_filters_filter').each(function() {
      if ($(this).find('.search_api_extended_filters_filter_conjunction').length > 0) {
        var wrapper = $(this).find('div.fieldset-wrapper');
        var select = $(
          '<select class="search_api_extended_filters_add_filter">'
          + '<option value="">- ' + Drupal.t('Add filter') + ' -</option>'
          + '<option value="AND">' + Drupal.t('AND') + '</option>'
          + '<option value="OR">' + Drupal.t('OR') + '</option>'
          + '</select>'
        );
        select.bind('change', function() {
          filter_container = search_api_extended_filters_add_filter($(this).val(), wrapper);
        });
        wrapper.append(select);
      }
    });

    $('.search_api_extended_filters_keys,.search_api_extended_filters_filter')
    .find('.search_api_extended_filters_filter_container')
    .each(function() {
      search_api_extended_filters_enable_autocomplete($(this));
    });
  });

})(jQuery);
