<?php
/**
 * @file
 * Views specific functions.
 */

/**
 * Implements hook_views_data_alter().
 */
function search_api_extended_filters_views_data_alter(&$data) {
  foreach (search_api_index_load_multiple(FALSE) as $index) {
    $key = 'search_api_index_' . $index->machine_name;

    $data[$key]['advanced_fulltext_search_fulltext']['group'] = t('Search');
    $data[$key]['advanced_fulltext_search_fulltext']['title'] = t('Advanced fulltext search');
    $data[$key]['advanced_fulltext_search_fulltext']['help'] = t('Search several or all fulltext fields at once.');
    $data[$key]['advanced_fulltext_search_fulltext']['filter']['handler'] = 'SearchApiExtendedHandlerFulltextFilter';
    $data[$key]['advanced_fulltext_search_fulltext']['argument']['handler'] = 'SearchApiViewsHandlerArgumentFulltext';

    // Create a new extended filter for each filter that is handled by
    // SearchApiViewsHandlerFilterOptions.
    foreach ($data[$key] as $k => &$v) {
      if (isset($v['filter']['handler'])) {
        if ($v['filter']['handler'] == 'SearchApiViewsHandlerFilterOptions') {
          $v['title'] = $v['title'] . ' (' . t('extended') . ')';
          $v['filter']['handler'] = 'SearchApiViewsHandlerFilterExtendedOptions';
        }
        elseif ($v['filter']['handler'] === 'SearchApiViewsHandlerFilterTaxonomyTerm') {
          $v['title'] .= ' (' . t('extended') . ')';
          $v['filter']['handler'] = 'SearchApiViewsHandlerFilterExtendedTaxonomyTerm';
        }
      }
    }

  }
}
