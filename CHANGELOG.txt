
Search API Extended Filters https://drupal.org/sandbox/AlexArnaud/1871208 -
CHANGELOG.txt
===============================================================================

2013-02-18	7.x-1.1
2013-01-23	7.x-1.0


Search API Extended Filters 7.x-1.1, 2013-02-18
-------------------------------------------------------------------------------

Enhancements:
  - (RM 749) Split filter strings into words, and combine them with 'AND'
    so that searching
      field = word1 word2
    will search
      (field:"word1" AND field:"word2")
    
    Double-quoted strings are treated as a single word.

  - (RM 723) Allow users to combine as many filters as they want
    Handle a dynamic number of filters.
    
    This patch adds a drop-down list that allow you to choose between 'AND'
    and 'OR' conjunctions. Once chosen, a new filter is created.

  - (RM 645) Add multiple filters with boolean conjunctions

Bugfixes:
  - (RM 738) Fix multiple errors like & Add comments to conform to Drupal coding standards
    Fix 'Filter term on unknown or unindexed field' error
    An exception was thrown when not all parameters were in the url
    
    Fix use of filter in 'keys' mode
    'keys' mode was completely broken
    
    add an option 'All fields' in fields drop-down list when filter is in
    'keys' mode
    
    add CSS to display the filter select in front of text input in
    'keys' mode


$ git sb 7.x-1.0 HEAD
! [7.x-1.0] Fix a wrong use of references in parent:options_form()
 ! [HEAD] R749: Update CHANGELOG.txt
--
 + [5a35da0] R749: Update CHANGELOG.txt
 + [c742fc8] R749: Split filter strings into words
 + [8154a66] R738: Add comments to conform to Drupal coding standards
 + [781d246] R723: Updated CHANGELOG.txt
 + [3a3598f] R738: Fix multiple errors
 + [3bd52bd] R723: Handle a dynamic number of filters.
 + [cd388aa] R645: Multiple filters with boolean conjunctions
 + [240f695] Initiate CHANGELOG.txt
++ [054d115] Fix a wrong use of references in parent:options_form()


Search API Extended Filters 7.x-1.0, 2013-01-23
-------------------------------------------------------------------------------

First release tag for search_api_extended_filters module :)

Information:
* Sandbox project http://drupal.org/sandbox/AlexArnaud/1871208

