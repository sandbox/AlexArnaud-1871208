<?php
/**
 * @file
 * AdvancedFulltextFilter class.
 */


/**
 * Views filter handler class for handling fulltext fields.
 */
class SearchApiExtendedHandlerFulltextFilter extends SearchApiViewsHandlerFilterFulltext {
  public function option_definition() {
    $options = parent::option_definition();

    $options['expose']['contains']['allow_filter_add'] = array('default' => 1);

    return $options;
  }

  /**
   * Modify the "value" option in expose form.
   *
   * Replace the "Searched fields" select by a 'tabledrag' where the user can
   * customize the order of fields.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['expose']['allow_filter_add'] = array(
      '#type' => 'radios',
      '#title' => t('Allow the addition of new filters'),
      '#options' => array(0 => t('No'), 1 => t('Yes')),
      '#default_value' => isset($this->options['expose']['allow_filter_add']) ? $this->options['expose']['allow_filter_add'] : 1,
    );

    $form['fields'] = array(
      '#type' => 'fieldset',
      '#title' => t("Searched fields"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#theme' => 'search_api_extended_filters_fulltext_fields',
    );

    // 17 is the length of 'search_api_index_' which is prefixed to index name.
    $index = search_api_index_load(substr($this->table, 17));
    $fields = array_intersect_key($index->getFields(), $this->getFulltextFields());
    $weight_delta = (count($fields) / 2) + 1;
    foreach ($fields as $fieldname => $field) {
      $options = isset($this->options['fields'][$fieldname]) ?
        $this->options['fields'][$fieldname] : NULL;

      $form['fields'][$fieldname]['label'] = array(
        '#markup' => $field['name'],
      );
      $form['fields'][$fieldname]['use'] = array(
        '#type' => 'checkbox',
        '#title' => t("Use this field ?"),
        '#default_value' => isset($options) ? $options['use'] : FALSE,
        '#attributes' => array(
          'title' => t("Use this field ?"),
        ),
      );
      $form['fields'][$fieldname]['weight'] = array(
        '#type' => 'weight',
        '#title' => t("Weight"),
        '#delta' => $weight_delta,
        '#default_value' => isset($options) ? $options['weight'] : 0,
      );
      $form['fields'][$fieldname]['default'] = array(
        '#type' => 'checkbox',
        '#title' => t("Default"),
        '#default_value' => isset($options) ? $options['default'] : FALSE,
        '#attributes' => array(
          'title' => t('Selected by default ?'),
        ),
      );
    }
  }

  /**
   * Create the conjunction form element.
   */
  protected function filterConjunction($value = NULL) {
    $element = array(
      '#type' => 'select',
      '#title' => t('Conjunction'),
      '#options' => array(
        'AND' => t('AND'),
        'OR' => t('OR'),
      ),
      '#default_value' => 'AND',
      '#attributes' => array(
        'class' => array('search_api_extended_filters_filter_conjunction'),
      ),
      '#value' => $value,
    );
    if (!isset($value)) {
      $element['#disabled'] = TRUE;
    }
    return $element;
  }

  /**
   * Create the field form element.
   */
  protected function filterField($value = NULL, $options = array()) {
    $fields = isset($options['fields']) ? $options['fields'] : array();
    $empty_option = isset($options['empty_option']) ? $options['empty_option'] : NULL;
    $element = array(
      '#type' => 'select',
      '#title' => t('Field'),
      '#options' => $fields,
      '#attributes' => array(
        'class' => array('search_api_extended_filters_filter_field'),
      ),
      '#value' => $value,
      '#empty_option' => $empty_option,
    );

    if (count($fields) === 1) {
      $element['#attributes']['style'] = 'display: none';
    }

    return $element;
  }

  /**
   * Create the value form element.
   */
  protected function filterValue($value = NULL) {
    $filter = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#default_value' => '',
      '#attributes' => array(
        'class' => array('search_api_extended_filters_filter_value'),
      ),
      '#value' => $value,
    );

    if (module_exists('search_api_autocomplete')) {
      $search_id = 'search_api_views_' . $this->view->name;
      $search = search_api_autocomplete_search_load($search_id);
      if ($search && $search->enabled) {
        $sapi_ac_path = drupal_get_path('module', 'search_api_autocomplete');
        $filter['#autocomplete_path'] = 'search_api_autocomplete/' . $search_id . '/PLACEHOLDER';
        $filter['#attached'] = array(
          'js' => array(
            $sapi_ac_path . '/search_api_autocomplete.js',
          ),
          'css' => array(
            $sapi_ac_path . '/search_api_autocomplete.css',
          ),
        );
      }
    }

    return $filter;
  }

  /**
   * Define filter form.
   */
  public function value_form(&$form, &$form_state) {
    $identifier = $this->options['expose']['identifier'];
    $values = array_key_exists($identifier, $form_state['input']) ? $form_state['input'][$identifier] : array(array());

    $fulltext_fields = $this->getFulltextFields();
    $fields = $this->options['fields'];

    $fields = array_filter($fields, function($field) {
      return $field['use'];
    });
    $default_fields = array_keys(array_filter($fields, function ($field) {
      return isset($field['default']) ? $field['default'] : 0;
    }));

    if (!count($fields)) {
      $fields = $this->options['fields'];
    }

    uasort($fields, function($a, $b) {
      return $a['weight'] - $b['weight'];
    });

    foreach ($fields as $fieldname => $field) {
      $fields[$fieldname] = $fulltext_fields[$fieldname];
    }
    $form['value'] = array();
    if (!empty($fields)) {
      $form['value'][$identifier] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#after_build' => array(
          'search_api_extended_filters_load_js',
          'search_api_extended_filters_load_css',
        ),
        '#attributes' => array(
          'class' => array('search_api_extended_filters_' . $this->options['mode']),
          'id' => $identifier,
        ),
      );
      if ($this->options['mode'] == 'filter') {
        $i = 0; $j = 0;
        while ($i < count($values)) {
          if ($i == 0 || !empty($values[$i]['value'])) {
            $form['value'][$identifier][$j] = array(
              '#type' => 'container',
              '#attributes' => array(
                'class' => array('search_api_extended_filters_filter_container'),
              ),
            );
            $conjunction = array_key_exists('conjunction', $values[$i]) ? $values[$i]['conjunction'] : NULL;
            $field = array_key_exists('field', $values[$i]) ? $values[$i]['field'] : $default_fields;
            $value = array_key_exists('value', $values[$i]) ? $values[$i]['value'] : NULL;

            if ($this->options['expose']['allow_filter_add']) {
              $form['value'][$identifier][$j]['conjunction'] = $this->filterConjunction($conjunction);
            }
            $form['value'][$identifier][$j]['field'] = $this->filterField($field, array('fields' => $fields));
            $form['value'][$identifier][$j]['value'] = $this->filterValue($value);
            $j++;
          }
          $i++;
        }
      }
      else {
        $form['value'][$identifier][0] = array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('search_api_extended_filters_filter_container'),
          ),
        );
        $conjunction = array_key_exists('conjunction', $values[0]) ? $values[0]['conjunction'] : NULL;
        $field = array_key_exists('field', $values[0]) ? $values[0]['field'] : $default_fields;
        $value = array_key_exists('value', $values[0]) ? $values[0]['value'] : NULL;

        $form['value'][$identifier][0]['field'] = $this->filterField($field, array('fields' => $fields, 'empty_option' => t('All fields')));
        $form['value'][$identifier][0]['value'] = $this->filterValue($value);
      }
    }
  }

  /**
   * Override parent::accept_exposed_input.
   */
  public function accept_exposed_input($input) {
    $this->value = $input[$this->options['expose']['identifier']];

    return TRUE;
  }

  /**
   * Add this filter to the query.
   */
  public function query() {
    $fields = $this->getFulltextFields();

    // If something already specifically set different fields, we silently fall
    // back to mere filtering.
    $filter = $this->options['mode'] == 'filter';
    if (!$filter) {
      $old = $this->query->getFields();
      $filter = $old && (array_diff($old, $fields) || array_diff($fields, $old));
    }

    if ($filter) {
      $filters = array();
      $values = $this->value;
      $i = 0; $j = 0;
      foreach ($values as &$value_ref) {
        $value = $value_ref['value'];
        // Split $value into words, but keep quoted strings as a single word.
        $value_ref['values'] = !empty($value) ? str_getcsv($value, ' ') : $value;
      }
      while ($i < count($values)) {
        $value = $values[$i];
        if (!empty($value['values'])) {
          $filters[$j] = $this->query->createFilter();
          $value_filter = $this->query->createFilter('AND');
          foreach ($value['values'] as $v) {
            $value_filter->condition($value['field'], $v, '=');
          }
          $filters[$j]->filter($value_filter);
          if ($j > 0) {
            $filters[$j - 1]->setConjunction($value['conjunction']);
            $filters[$j - 1]->filter($filters[$j]);
          }
          $j++;
        }
        $i++;
      }
      if (count($filters)) {
        $this->query->filter($filters[0]);
      }

      return;
    }

    $searched_field = $this->value[0]['field'];
    $this->value = $this->value[0]['value'];
    if (isset($fields[$searched_field])) {
      foreach ($fields as $field_name => $field) {
        if ($field_name != $searched_field) {
          unset($fields[$field_name]);
        }
      }
    }

    $this->query->fields(array_keys($fields));
    $old = $this->query->getOriginalKeys();
    $this->query->keys($this->value);
    if ($this->operator != '=') {
      $keys = &$this->query->getKeys();
      if (is_array($keys)) {
        $keys['#negation'] = TRUE;
      }
      else {
        // We can't know how negation is expressed in the server's syntax.
      }
    }
    if ($old) {
      $keys = &$this->query->getKeys();
      if (is_array($keys)) {
        $keys[] = $old;
      }
      elseif (is_array($old)) {
        // We don't support such nonsense.
      }
      else {
        $keys = "($old) ($keys)";
      }
    }
  }
}

/**
 * Load JS files for this filter.
 */
function search_api_extended_filters_load_js($element) {
  drupal_add_js(drupal_get_path('module', 'search_api_extended_filters') . '/js/filter_extended_fulltext.js');
  return $element;
}

/**
 * Load CSS files for this filter.
 */
function search_api_extended_filters_load_css($element) {
  drupal_add_css(drupal_get_path('module', 'search_api_extended_filters') . '/css/filter_extended_fulltext.css');
  return $element;
}
