<?php

/**
 * @file
 * SearchApiViewsHandlerFilterExtendedOptions definition file.
 */

class SearchApiViewsHandlerFilterExtendedOptions extends SearchApiViewsHandlerFilterOptions {

  /**
   * Modify the "value" option in expose form.
   */
  public function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);

    $value_form_element = $form['value'];
    unset($form['value']);
    $form['value_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => 'Default value',
      '#weight' => -2,
      '#states' => $value_form_element['#states'],
    );
    $form['value_fieldset']['value_type'] = array(
      '#type' => 'radios',
      '#options' => array(
        'fixed' => t('Fixed value'),
        'dynamic' => t('Dynamic value (PHP code)'),
      ),
      '#default_value' => isset($this->options['value_type']) ? $this->options['value_type'] : 'fixed',
    );

    $form['value_fieldset']['fixed_value'] = array(
      '#title' => NULL,
      '#default_value' => isset($this->options['fixed_value']) ? $this->options['fixed_value'] : array(),
      '#prefix' => '<div class="form-item">',
      '#suffix' => '</div>',
    ) + $value_form_element;

    $form['value_fieldset']['fixed_value']['#states']['visible'] = array(
      ':input[name="options[value_fieldset][value_type]"]' => array('value' => 'fixed'),
    );

    $form['value_fieldset']['dynamic_value'] = array(
      '#type' => 'textarea',
      '#default_value' => isset($this->options['dynamic_value']) ? $this->options['dynamic_value'] : 'return array();',
      '#states' => array(
        'visible' => array(
          ':input[name="options[value_fieldset][value_type]"]' => array('value' => 'dynamic'),
        ),
      ),
    );

    $reduce_form_element = $form['expose']['reduce'];
    unset($form['expose']['reduce']);
    $form['expose']['reduce_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Limit list'),
      '#weight' => -1,
    );
    $form['expose']['reduce_fieldset']['reduce'] = $reduce_form_element;
    $form['expose']['reduce_fieldset']['reduce_values'] = array(
      '#title' => NULL,
      '#default_value' => isset($this->options['expose']['reduce_values']) ? $this->options['expose']['reduce_values'] : array(),
      '#prefix' => '<div class="form-item">',
      '#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          ':input[name="options[expose][reduce_fieldset][reduce]"]' => array('checked' => TRUE),
        ),
      ),
    ) + $value_form_element;

    $form['expose']['use_dropdownchecklist'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use jQuery Dropdown CheckList plugin"),
      '#description' => t("It requires jquery_update module jQuery version 1.6 or greater."),
      '#default_value' => isset($this->options['expose']['use_dropdownchecklist']) ? $this->options['expose']['use_dropdownchecklist'] : 0,
      '#id' => 'use-dropdownchecklist',
    );

    $form['expose']['dropdownchecklist'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dropdown CheckList options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => array(
        'visible' => array(
          '#use-dropdownchecklist' => array('checked' => TRUE),
        ),
      ),
    );
    $form['expose']['dropdownchecklist']['add_all_option'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add "Select all" option'),
      '#default_value' => isset($this->options['expose']['dropdownchecklist']['add_all_option']) ? $this->options['expose']['dropdownchecklist']['add_all_option'] : 0,
    );
    $form['expose']['dropdownchecklist']['max_items_displayed'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of items displayed at once'),
      '#description' => t("Minimum allowed is 4"),
      '#default_value' => isset($this->options['expose']['dropdownchecklist']['max_items_displayed']) ? $this->options['expose']['dropdownchecklist']['max_items_displayed'] : '',
    );
  }

  /**
   * Return the reduced list of options.
   */
  public function reduce_value_options() {
    $options = array();
    foreach ($this->value_options as $id => $option) {
      if (isset($this->options['expose']['reduce_values'][$id]) && $this->options['expose']['reduce_values'][$id]) {
        $options[$id] = $option;
      }
    }
    return $options;
  }

  /**
   * Modify default value of filter.
   */
  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    // Do not try to eval code if we're in views filter configuration form.
    // This way we can edit options even if code is buggy/malformated.
    if (!empty($form_state['exposed'])) {
      $default_value = $form['value']['#default_value'];
      $form['value']['#value_callback'] = 'search_api_extended_filters_extended_options_value';
      if ($this->options['value_type'] == 'dynamic') {
        $default_value = eval($this->options['value']);
        $default_value = drupal_map_assoc($default_value);
      }
      $options = $this->options['expose']['reduce'] ? $this->reduce_value_options() : $this->value_options;
      $form['value']['#options'] = $options;
      // Ensure that default values are present in options.
      $form['value']['#default_value'] = array_intersect_key($default_value, $options);

      // Enable dropdownchecklist plugin only if jQuery version
      // is 1.6 or greater.
      $use_dropdownchecklist = isset($this->options['expose']['use_dropdownchecklist']) ? $this->options['expose']['use_dropdownchecklist'] : 0;
      if ($use_dropdownchecklist && module_exists('jquery_update')) {
        $version = variable_get('jquery_update_jquery_version', '1.5');
        if ($version >= 1.6) {
          drupal_add_library('system', 'ui.widget');
          $path = drupal_get_path('module', 'search_api_extended_filters');
          $form['value']['#attached'] = array(
            'js' => array(
              $path . '/js/ui.dropdownchecklist-1.4-min.js',
              $path . '/js/filter_extended_options.js',
            ),
            'css' => array(
              $path . '/css/ui.dropdownchecklist.standalone.css',
              $path . '/css/filter_extended_options.css',
            ),
          );
          drupal_add_js(array(
            'search_api_extended_filters' => array(
              $this->options['id'] => array(
                'identifier' => $this->options['expose']['identifier'],
                'use_dropdownchecklist' => TRUE,
                'add_all_option' => $this->options['expose']['dropdownchecklist']['add_all_option'],
                'max_items_displayed' => $this->options['expose']['dropdownchecklist']['max_items_displayed'],
              ),
            ),
          ), 'setting');
          if ($this->options['expose']['dropdownchecklist']['add_all_option']) {
            $form['value']['#options'] = array('' => t('All')) + $form['value']['#options'];
          }
          $form['value']['#attributes']['class'] = array(
            'search_api_extended_filters_dropdownchecklist',
          );
        }
      }
    }
  }

  /**
   * Save value options.
   */
  public function value_submit($form, &$form_state) {
    parent::value_submit($form, $form_state);

    $value = array();
    $options = $form_state['values']['options'];
    $value_fieldset = $options['value_fieldset'];
    if (isset($value_fieldset)) {
      $form_state['values']['options']['value_type'] = $value_fieldset['value_type'];
      $form_state['values']['options']['fixed_value'] = $value_fieldset['fixed_value'];
      $form_state['values']['options']['dynamic_value'] = $value_fieldset['dynamic_value'];
      $form_state['values']['options']['expose']['reduce'] = $options['expose']['reduce_fieldset']['reduce'];
      $form_state['values']['options']['expose']['reduce_values'] = $options['expose']['reduce_fieldset']['reduce_values'];
      $value = ($value_fieldset['value_type'] == 'fixed') ? $value_fieldset['fixed_value'] : $value_fieldset['dynamic_value'];

    }
    else {
      $value = $form_state['values']['options']['value'];
    }
    $form_state['values']['options']['value'] = $value;
  }

  /**
   * Define new options.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['value_type'] = array('default' => 'fixed');
    $options['fixed_value'] = array('default' => array());
    $options['dynamic_value'] = array('default' => 'return array();');
    $options['expose']['contains']['reduce_values'] = array('default' => array());
    $options['expose']['contains']['use_dropdownchecklist'] = array('default' => 0);
    $options['expose']['contains']['dropdownchecklist']['add_all_option'] = array('default' => 0);
    $options['expose']['contains']['dropdownchecklist']['max_items_displayed'] = array('default' => '');

    return $options;
  }
}

/**
 * Callback for settings value of filter.
 *
 * This a slightly modified version of form_type_select_value.
 *
 * This is needed because with Views+BEF, Drupal is thinking the form has been
 * submitted, even if "Input required" option is ON, but $input is NULL so
 * default value becomes array() instead of $element['#default_value'].
 * The counterpart of this is that if user deselect all options and submit the
 * form, the default values come back in place.
 */
function search_api_extended_filters_extended_options_value(&$element, $input = FALSE, $form_state = array()) {
  if ($input !== FALSE) {
    if (isset($element['#multiple']) && $element['#multiple']) {
      if (is_array($input) && count($input) > 0) {
        return drupal_map_assoc($input);
      }
      else {
        // If we are here, we have two possibilities:
        // 1/ the form was not submitted yet, or
        // 2/ no values have been selected for select/checkboxes
        // We use exposed_filter_applied to know if form was submitted or not.
        $exposed_form_plugin = $form_state['exposed_form_plugin'];
        $exposed_filter_applied = FALSE;
        if (method_exists($exposed_form_plugin, 'exposed_filter_applied')) {
          $exposed_filter_applied = $exposed_form_plugin->exposed_filter_applied();
        }
        $value = array();
        if (!$exposed_filter_applied) {
          // If form was not submitted yet, use default values.
          if (isset($element['#default_value']) && is_array($element['#default_value'])) {
            // Do not return zero values.
            $value = array_filter($element['#default_value'], function($v) {
              return ($v !== 0) ? TRUE : FALSE;
            });
          }
        }
        $element['#default_value'] = $value;
        return $element['#default_value'];
      }
    }
    elseif (isset($element['#empty_value']) && $input === (string) $element['#empty_value']) {
      return $element['#empty_value'];
    }
    else {
      return $input;
    }
  }
}
