<?php

/**
 * @file
 * SearchApiViewsHandlerFilterExtendedTaxonomyTerm definition file.
 */

class SearchApiViewsHandlerFilterExtendedTaxonomyTerm extends SearchApiViewsHandlerFilterTaxonomyTerm {

  /**
   * Modify the "value" option in expose form.
   */
  public function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);

    $reduce_form_element = $form['expose']['reduce'];
    unset($form['expose']['reduce']);
    $form['expose']['reduce_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Limit list'),
      '#weight' => -1,
    );
    $form['expose']['reduce_fieldset']['reduce'] = $reduce_form_element;
    $form['expose']['reduce_fieldset']['reduce_values'] = array(
      '#title' => NULL,
      '#default_value' => isset($this->options['expose']['reduce_values']) ? $this->options['expose']['reduce_values'] : array(),
      '#prefix' => '<div class="form-item">',
      '#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          ':input[name="options[expose][reduce_fieldset][reduce]"]' => array('checked' => TRUE),
        ),
      ),
    ) + $form['value'];

    $form['expose']['use_dropdownchecklist'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use jQuery Dropdown CheckList plugin"),
      '#description' => t("It requires jquery_update module jQuery version 1.6 or greater."),
      '#default_value' => isset($this->options['expose']['use_dropdownchecklist']) ? $this->options['expose']['use_dropdownchecklist'] : 0,
      '#id' => 'use-dropdownchecklist',
    );

    $form['expose']['dropdownchecklist'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dropdown CheckList options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => array(
        'visible' => array(
          '#use-dropdownchecklist' => array('checked' => TRUE),
        ),
      ),
    );
    $form['expose']['dropdownchecklist']['add_all_option'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add "Select all" option'),
      '#default_value' => isset($this->options['expose']['dropdownchecklist']['add_all_option']) ? $this->options['expose']['dropdownchecklist']['add_all_option'] : 0,
    );
    $form['expose']['dropdownchecklist']['max_items_displayed'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of items displayed at once'),
      '#description' => t("Minimum allowed is 4"),
      '#default_value' => isset($this->options['expose']['dropdownchecklist']['max_items_displayed']) ? $this->options['expose']['dropdownchecklist']['max_items_displayed'] : '',
    );
  }

  /**
   * Return the reduced list of options.
   */
  public function reduce_value_options(array $options) {
    foreach ($options as $id => $option) {
      if (!isset($this->options['expose']['reduce_values'][$id]) || !$this->options['expose']['reduce_values'][$id]) {
        unset($options[$id]);
      }
    }
    return $options;
  }

  /**
   * Modify default value of filter.
   */
  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    // Do not try to eval code if we're in views filter configuration form.
    // This way we can edit options even if code is buggy/malformated.
    if (!empty($form_state['exposed'])) {

      // Enable dropdownchecklist plugin only if jQuery version
      // is 1.6 or greater.
      $use_dropdownchecklist = isset($this->options['expose']['use_dropdownchecklist']) ? $this->options['expose']['use_dropdownchecklist'] : 0;
      if ($use_dropdownchecklist && module_exists('jquery_update')) {
        $version = variable_get('jquery_update_jquery_version', '1.5');
        if (version_compare($version, 1.6) >= 0) {
          drupal_add_library('system', 'ui.widget');
          $path = drupal_get_path('module', 'search_api_extended_filters');
          $form['value']['#attached'] = array(
            'js' => array(
              $path . '/js/ui.dropdownchecklist-1.4-min.js',
              $path . '/js/filter_extended_options.js',
            ),
            'css' => array(
              $path . '/css/ui.dropdownchecklist.standalone.css',
              $path . '/css/filter_extended_options.css',
            ),
          );
          drupal_add_js(array(
            'search_api_extended_filters' => array(
              $this->options['id'] => array(
                'use_dropdownchecklist' => TRUE,
                'add_all_option' => $this->options['expose']['dropdownchecklist']['add_all_option'],
                'max_items_displayed' => $this->options['expose']['dropdownchecklist']['max_items_displayed'],
              ),
            ),
          ), 'setting');
          if ($this->options['expose']['dropdownchecklist']['add_all_option']) {
            $form['value']['#options'] = array('' => t('All')) + $form['value']['#options'];
          }
          $form['value']['#attributes']['class'] = array(
            'search_api_extended_filters_dropdownchecklist',
          );
        }
      }
    }
  }

  /**
   * Save value options.
   */
  public function value_submit($form, &$form_state) {
    parent::value_submit($form, $form_state);

    $value = array();
    $options = $form_state['values']['options'];
    $reduce_fieldset = $options['expose']['reduce_fieldset'];
    if (isset($reduce_fieldset)) {
      $form_state['values']['options']['expose']['reduce'] = $reduce_fieldset['reduce'];
      $form_state['values']['options']['expose']['reduce_values'] = $reduce_fieldset['reduce_values'];

    }
  }

  /**
   * Define new options.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['expose']['contains']['reduce_values'] = array('default' => array());
    $options['expose']['contains']['use_dropdownchecklist'] = array('default' => 0);
    $options['expose']['contains']['dropdownchecklist']['add_all_option'] = array('default' => 0);
    $options['expose']['contains']['dropdownchecklist']['max_items_displayed'] = array('default' => '');

    return $options;
  }
}
